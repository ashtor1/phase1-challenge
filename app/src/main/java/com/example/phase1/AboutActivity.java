package com.example.phase1;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AboutActivity extends AppCompatActivity {

    WebView webView;
    MyWebViewClient myWebViewClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        webView = (WebView) findViewById(R.id.wvAbout);
        String url = "https://andela.com/alc/";
        webView.getSettings().setJavaScriptEnabled(true);
        myWebViewClient = new MyWebViewClient();
        webView.setWebViewClient(myWebViewClient);
        webView.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            String message = "SSL Certificate error.";
            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "The certificate authority is not trusted.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
            }
            message += "\"SSL Certificate Error\" Do you want to continue anyway?.. YES";

            handler.proceed();
        }

//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if(Uri.parse(url).getHost().equals("https://andela.com/alc/")) {
//                return true;
//            }
//            return true;
//        }

    }
}
